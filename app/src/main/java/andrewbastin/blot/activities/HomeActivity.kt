package andrewbastin.blot.activities

import andrewbastin.blot.R
import andrewbastin.blot.extensions.transaction
import andrewbastin.blot.fragments.home.MePageFragment
import andrewbastin.blot.fragments.home.MyFeedPageFragment
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import com.yarolegovich.slidingrootnav.SlidingRootNav
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder
import kotlinx.android.synthetic.main.layout_home_sidebar.*

class HomeActivity : AppCompatActivity() {

    private lateinit var sidebarButtons: List<Button>
    private var currentIndex = 1 // my feed by default

    private lateinit var slidingRootNav: SlidingRootNav

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }

        setContentView(R.layout.activity_home)

        slidingRootNav = SlidingRootNavBuilder(this).apply {
            withMenuLayout(R.layout.layout_home_sidebar)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) addDragListener(this@HomeActivity::slidingRootNavDragListener)
        }.inject()

        setUpMenuActions()

        updateContentView(false)
    }

    private fun slidingRootNavDragListener(progress: Float) {
        // Switch Status Bar theme (>= Marshmallow)
        if (progress > 0.5) {
            window.decorView.systemUiVisibility = window.decorView.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        } else {
            window.decorView.systemUiVisibility = window.decorView.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
        }
    }

    private fun setUpMenuActions() {
        sidebarButtons = listOf(homeSidebarDirect, homeSidebarMyFeed, homeSidebarMe, homeSidebarTrending, homeSidebarTrending, homeSidebarNearYou, homeSidebarSearch, homeSidebarNotifications, homeSidebarSettings)

        sidebarButtons.forEachIndexed { index, button ->
            button.setOnClickListener {
                onSidebarClick(index)
            }
        }
    }

    private fun onSidebarClick(index: Int) {
        sidebarButtons.forEachIndexed { i, button ->
            if (i == index) button.setTextColor(Color.BLACK)
            else button.setTextColor(Color.rgb(169, 169, 169))
        }
        currentIndex = index
        slidingRootNav.closeMenu(true)
        updateContentView()
    }

    private fun updateContentView(addToBackStack: Boolean = true) {
        supportFragmentManager.transaction {
            replace(R.id.homeContentView, getContentFragment(currentIndex))

            if (addToBackStack) addToBackStack("homeBackStack") else addToBackStack(null)
        }
    }

    private fun getContentFragment(id: Int): Fragment {
        return when (id) {

            1 -> MyFeedPageFragment.createInstance()
            2 -> MePageFragment.createInstance()

            // TODO : Implement other indexes as well
            else -> MyFeedPageFragment.createInstance()
        }
    }
}
