package andrewbastin.blot.activities

import andrewbastin.blot.R
import andrewbastin.blot.api.auth.APIAuth
import andrewbastin.blot.extensions.startActivityAndClearBackStack
import andrewbastin.blot.persistance.Prefs
import andrewbastin.blot.persistance.SharedValues
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }

        setContentView(R.layout.activity_login)

        backButtonLogin.setOnClickListener {
            finish()
        }

        log_inButton.setOnClickListener {
            log_inButton.visibility = View.GONE
            loginProgressBar.visibility = View.VISIBLE

            APIAuth.loginWithAuth(usernameLoginTextBox.text.toString(), usernameLoginTextBox.text.toString()) { sessionToken, res ->

                if (sessionToken != null && res != null && res.isSuccessful) {
                    APIAuth.getLoginToken(sessionToken) { loginToken, resp ->

                        if (loginToken != null && resp != null && resp.isSuccessful) {

                            Prefs.AuthPrefs.loginToken = loginToken
                            SharedValues.sessionToken = sessionToken

                            this.runOnUiThread {
                                startActivityAndClearBackStack(HomeActivity::class.java)
                            }

                        }

                        this.runOnUiThread {
                            log_inButton.visibility = View.VISIBLE
                            loginProgressBar.visibility = View.GONE
                        }
                    }
                } else {
                    this.runOnUiThread {
                        log_inButton.visibility = View.VISIBLE
                        loginProgressBar.visibility = View.GONE
                    }
                }



            }
        }
    }
}