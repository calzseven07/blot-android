package andrewbastin.blot.activities
import andrewbastin.blot.R
import andrewbastin.blot.api.auth.APIAuth
import andrewbastin.blot.extensions.isDeviceOnline
import andrewbastin.blot.extensions.startActivity
import andrewbastin.blot.persistance.Prefs
import andrewbastin.blot.persistance.SharedValues
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_init.*
import kotlinx.android.synthetic.main.activity_init.initActivityJoinButton as joinButton
import kotlinx.android.synthetic.main.activity_init.initActivityButtonLayout as buttonLayout
import kotlinx.android.synthetic.main.activity_init.initActivityLoginButton as loginButton


class InitActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Prefs.loadPrefs(this)

        // Status bar transparency (>= API 21)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }

        setContentView(R.layout.activity_init)


        // region Listeners
        loginButton.setOnClickListener {
            startActivity(LoginActivity::class.java)
        }

        retry_btn.setOnClickListener {
            autoLogin()
        }

        joinButton.setOnClickListener {
            startActivity(JoinActivity::class.java)
        }
        // endregion

        autoLogin()

    }

    private fun autoLogin() {

        if (isDeviceOnline()) {
            retry_btn.visibility = View.GONE
            check_int_btn.visibility = View.GONE
            buttonLayout.visibility = View.VISIBLE
        } else {
            check_int_btn.visibility = View.VISIBLE
            retry_btn.visibility = View.VISIBLE
            buttonLayout.visibility = View.GONE
            return
        }

        if (Prefs.AuthPrefs.loginToken != "") {
            buttonLayout.visibility = View.GONE
            loadProgress.visibility = View.VISIBLE
            loggingInText.visibility = View.VISIBLE
            APIAuth.loginWithToken(Prefs.AuthPrefs.loginToken) { sessionToken, res ->
                if (sessionToken != null && res != null && res.isSuccessful) {
                    SharedValues.sessionToken = sessionToken

                    startActivity(HomeActivity::class.java)
                    finish()
                } else {
                    this.runOnUiThread {
                        buttonLayout.visibility = View.GONE
                        loadProgress.visibility = View.GONE
                        loggingInText.visibility = View.GONE
                    }
                }
            }
        }
    }

}






























