package andrewbastin.blot.api.auth

import andrewbastin.blot.extensions.encodeBase64
import andrewbastin.blot.singletons.httpClient
import okhttp3.*
import java.io.IOException

object APIAuth {

    fun loginWithAuth(username: String, password: String, onDone: (sessionToken: String?, res: Response?) -> Unit) {

        val authToken = "$username:$password".encodeBase64()

        val request = Request.Builder().apply {
            headers(Headers.of(mapOf("Authorization" to "Basic $authToken")))
            url("https://blot-backend.herokuapp.com/auth/loginWithAuth")
            get()

        }.build()

        httpClient.newCall(request).enqueue(
                object: Callback {
                    override fun onFailure(call: Call?, e: IOException?) {
                        onDone(null, null)
                    }

                    override fun onResponse(call: Call?, response: Response?) {
                        onDone(response?.body()?.string(), response)
                    }
                }

        )


    }

    fun getLoginToken(sessionToken: String, onDone: (loginToken: String?, res: Response?) -> Unit) {

        val request = Request.Builder().apply {
            headers(Headers.of(mapOf("Authorization" to sessionToken)))
            url("https://blot-backend.herokuapp.com/auth/createLoginToken")
            get()
        }.build()

        httpClient.newCall(request).enqueue(
                object:Callback {

                    override fun onFailure(call: Call?, e: IOException?) {
                        onDone(null, null)
                    }

                    override fun onResponse(call: Call?, response: Response?) {
                        onDone(response?.body()?.string(), response)
                    }

                }
        )

    }

    fun loginWithToken(loginToken: String, onDone: (sessionToken: String?, res: Response?) -> Unit) {

        val request = Request.Builder().apply {
            headers(Headers.of(mapOf("Authorization" to loginToken)))
            url("https://blot-backend.herokuapp.com/auth/loginWithToken")
            get()
        }.build()

        httpClient.newCall(request).enqueue(
                object: Callback {

                    override fun onFailure(call: Call?, e: IOException?) {
                        onDone(null, null)
                    }

                    override fun onResponse(call: Call?, response: Response?) {
                        onDone(response?.body()?.string(), response)
                    }

                }
        )

    }

}