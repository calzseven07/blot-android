package andrewbastin.blot.persistance

import android.content.Context

class AuthPrefs(context: Context) : SharedPrefs("auth", context) {

    var loginToken by stringPref("string_token_login", "")

}