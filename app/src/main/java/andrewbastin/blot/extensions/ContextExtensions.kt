package andrewbastin.blot.extensions

import android.content.Context
import android.net.ConnectivityManager

fun Context.isDeviceOnline(): Boolean {
    val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val actNetworkInfo = connectivityManager.activeNetworkInfo
    return actNetworkInfo != null && actNetworkInfo.isConnected
}