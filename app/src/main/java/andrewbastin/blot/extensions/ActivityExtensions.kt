package andrewbastin.blot.extensions

import android.app.Activity
import android.content.Intent

fun <T: Activity> Activity.startActivity(activityClass: Class<T>) {
    this.startActivity(Intent(this, activityClass))
}

fun <T: Activity> Activity.startActivityWithIntentFlag(activityClass: Class<T>, flag: Int) {
    val intent = Intent(this, activityClass)
    intent.flags = flag
    startActivity(intent)
}

fun <T: Activity> Activity.startActivityAndClearBackStack(activityClass: Class<T>) {
    startActivityWithIntentFlag(activityClass, Intent.FLAG_ACTIVITY_CLEAR_TOP)
}