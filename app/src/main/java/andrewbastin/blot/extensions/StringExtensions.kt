package andrewbastin.blot.extensions

import android.util.Base64

fun String.encodeBase64(): String {
    return Base64.encodeToString(this.toByteArray(Charsets.ISO_8859_1), Base64.NO_WRAP)
}

fun String.decodeBase64(): String {
    return Base64.decode(this, Base64.NO_WRAP).toString(Charsets.ISO_8859_1)
}