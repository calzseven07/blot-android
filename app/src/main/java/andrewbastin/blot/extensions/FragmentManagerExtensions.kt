package andrewbastin.blot.extensions

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction

fun FragmentManager.transaction(func: FragmentTransaction.() -> Unit) {
    val transaction = this.beginTransaction()
    transaction.func()
    transaction.commit()
}