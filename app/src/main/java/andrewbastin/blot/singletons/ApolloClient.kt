package andrewbastin.blot.singletons

import com.apollographql.apollo.ApolloClient

val gqlClient = ApolloClient.builder().apply {

    okHttpClient(gqlHTTPClient)
    serverUrl("https://blot-backend.herokuapp.com/")


}.build()