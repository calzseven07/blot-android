package andrewbastin.blot.singletons

import andrewbastin.blot.persistance.SharedValues
import okhttp3.OkHttpClient

val httpClient: OkHttpClient = OkHttpClient.Builder().apply {

    followRedirects(true)
    followSslRedirects(true)

}.build()

val gqlHTTPClient: OkHttpClient = OkHttpClient.Builder().apply {

    followRedirects(true)
    followSslRedirects(true)

    addInterceptor {
        val orig = it.request()
        it.proceed(orig.newBuilder().apply {

            SharedValues.sessionToken?.let {

                header("Authorization", it)

            }

        }.build())
    }

}.build()