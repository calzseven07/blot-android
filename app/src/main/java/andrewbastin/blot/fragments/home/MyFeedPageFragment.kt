package andrewbastin.blot.fragments.home

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import andrewbastin.blot.R

class MyFeedPageFragment : Fragment() {

    companion object {

        fun createInstance() = MyFeedPageFragment()

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_page_myfeed, container, false)
    }

}
